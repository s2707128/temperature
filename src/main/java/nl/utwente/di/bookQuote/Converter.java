package nl.utwente.di.bookQuote;

public class Converter {

    public double getFah(String isbn){
        double celsius = Double.valueOf(isbn);
        return (celsius * 1.8) + 32;
    }
}
